# QuoraPy


## Installation:
It is recomended to use via pyenv We will be supporting python 3.6.0 and above going forward

```
pip install --upgrade pip
curl https://pyenv.run | bash
curl -L https://quora.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
pyenv install 3.6.0
pyenv local 3.6.0
pip install -r requirements.txt
```

##  APIs:
  - [Follow followers of each of the users from a list](#Follow-followers-of-each-of-the-users-from-a-list)
  - [Follow following of each of the users from a list](#Follow-following-of-each-of-the-users-from-a-list)
  - [Unfollow users](#Unfollow-users)
  - [get profiles from search](#get-profiles-from-search)
  - [follow users from profile links](#follow-users-from-profile-links)
  - [get posts from search](#get-post-links-from-search)
  - [upvote from post links](#upvote-from-post-links)

### Follow followers of each of the users from a list

```python

 session = QuoraPy()

 with smart_run(session):
     session.follow_user_followers(my_userids_list,
                                  amount=random.randint(30, 60))
 ```
 
### Follow following of each of the users from a list

```python

 session = QuoraPy()

 with smart_run(session):
     session.follow_user_following(my_userids_list,
                                  amount=random.randint(30, 60))
 ```
 
### Unfollow users

```python

 session = QuoraPy()

 with smart_run(session):
     session.unfollow_users(amount=random.randint(30, 60))
 ```

### get profiles from search

```python

 session = QuoraPy()

with smart_run(session):
	profiles = session.get_profiles_from_search(search_term="happy")
```

### follow users from profile links

```python

 session = QuoraPy()

with smart_run(session):
	session.follow_users_from_profile_links(profile_links)
```


### get post links from search

```python

 session = QuoraPy()

with smart_run(session):
	post_links = get_post_links_from_search(search_term="cricket")
```

### upvote from post links

```python

 session = QuoraPy()

with smart_run(session):
    session.upvote_from_post_links(post_links)

```

## How to run:

 -  modify `quickstart.py` according to your requirements
 -  `python quickstart.py -u <my_email> -p <mypssword> -ui <my_quora_userid_not_username>`


## How to schedule as a job:

```bash
    */10 * * * * bash /path/to/QuoraPy/run_quorapy_only_once_for_mac.sh /path/to/QuoraPy/quickstart.py $EMAIL $PASSWORD $USERID
```

## Help build socialbotspy
Check out this short guide on [how to start contributing!](https://github.com/InstaPy/instapy-docs/blob/master/CONTRIBUTORS.md).

