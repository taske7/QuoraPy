""" Quickstart script for QuoraPy usage """

# imports
from quorapy import QuoraPy
from quorapy import smart_run
from socialcommons.file_manager import set_workspace
from quorapy import settings

import random
import datetime
now = datetime.datetime.now()

# set workspace folder at desired location (default is at your home folder)
set_workspace(settings.Settings, path=None)

# get an QuoraPy session!
session = QuoraPy(use_firefox=True)

with smart_run(session):
    """ Activity flow """
    # prepare data
    target_userids_4_copying_followers = ['Dream11-Fantasy-Sports', 'Myteam11-Fantasy-Sports', 'HalaPlay-4', 'Syed-Abad-1', 'Sandy-1120', 'Amit-1784', 'Rahul-Dev-819']
    number = random.randint(2, 3)
    if len(target_userids_4_copying_followers) <= number:
        random_target_userids_4_copying_followers = target_userids_4_copying_followers
    else:
        random_target_userids_4_copying_followers = random.sample(target_userids_4_copying_followers, number)

    # # general settings
    session.set_do_follow(enabled=True, percentage=40, times=1)
    session.set_dont_include(target_userids_4_copying_followers)
    followers, following = session.get_relationship_counts()

    # activity
    if 20 * followers < following:
        session.unfollow_users(skip = following/2, amount=random.randint(20, 30))
    session.follow_user_followers(random_target_userids_4_copying_followers, amount=random.randint(10, 20))
    session.follow_user_following(random_target_userids_4_copying_followers, amount=random.randint(10, 20))

    prof_links = session.get_profile_links_from_search(search_term="dream11")
    prof_links.extend(session.get_profile_links_from_search(search_term="myteam11"))
    prof_links.extend(session.get_profile_links_from_search(search_term="halaplay"))
    prof_links.extend(session.get_profile_links_from_search(search_term="dream11 expert"))
    prof_links.extend(session.get_profile_links_from_search(search_term="gamezy"))
    prof_links.extend(session.get_profile_links_from_search(search_term="fantasy sports"))
    prof_links.extend(session.get_profile_links_from_search(search_term="fantasy cricket"))
    random.shuffle(prof_links)
    session.follow_users_from_profile_links(prof_links)

    post_links = session.get_post_links_from_search(search_term="dream11")
    post_links.extend(session.get_post_links_from_search(search_term="myteam11"))
    post_links.extend(session.get_post_links_from_search(search_term="halaplay"))
    post_links.extend(session.get_post_links_from_search(search_term="dream11 expert"))
    post_links.extend(session.get_post_links_from_search(search_term="gamezy"))
    post_links.extend(session.get_post_links_from_search(search_term="fantasy sports"))
    post_links.extend(session.get_post_links_from_search(search_term="fantasy cricket"))
    random.shuffle(post_links)
    session.upvote_from_post_links(post_links)

